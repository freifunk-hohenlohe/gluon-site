# Gluon site config for Freifunk Hohenlohe

## requirements
ecdsautils are needed for signing

## build
```
export GLUON_BRANCH=experimental
export GLUON_TARGET=ar71xx-generic

#clone gluon
git clone https://github.com/freifunk-gluon/gluon.git -b v2016.2.2 gluon
cd gluon

#clone this repo
mkdir site
(cd site && git clone https://gitlab.com/freifunk-hohenlohe/gluon-site.git .)

#update sources
make update
make clean

#run build
make -j9

#optional: create manifest and sign it
make -j9 manifest
contrib/sign.sh ../ecdsa.key output/images/sysupgrade/${GLUON_BRANCH}.manifest
```

## more docs
https://gluon.readthedocs.org/
https://wiki.freifunk.net/ECDSA_Util
