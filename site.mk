GLUON_SITE_PACKAGES := \
	gluon-mesh-batman-adv-15 \
	gluon-alfred \
	gluon-respondd \
	gluon-autoupdater \
	gluon-config-mode-autoupdater \
	gluon-config-mode-contact-info \
	gluon-config-mode-core \
	gluon-config-mode-geo-location \
	gluon-config-mode-hostname \
	gluon-config-mode-mesh-vpn \
	gluon-ebtables-filter-multicast \
	gluon-ebtables-filter-ra-dhcp \
	gluon-luci-admin \
	gluon-luci-autoupdater \
	gluon-luci-portconfig \
	gluon-luci-wifi-config \
        gluon-luci-private-wifi \
	gluon-next-node \
	gluon-mesh-vpn-fastd \
	gluon-setup-mode \
	gluon-status-page \
	haveged \
	iptables \
	iwinfo

DEFAULT_GLUON_BRANCH := experimental
#DEFAULT_GLUON_BRANCH := beta
#DEFAULT_GLUON_BRANCH := stable

# Allow overriding the release number from the command line
GLUON_BRANCH ?= $(DEFAULT_GLUON_BRANCH)

GLUON_VERSION := 0.1.2

DEFAULT_GLUON_RELEASE := $(GLUON_VERSION)~$(GLUON_BRANCH)$(shell date '+%Y%m%d')

# Allow overriding the release number from the command line
GLUON_RELEASE ?= $(DEFAULT_GLUON_RELEASE)

# Default priority for updates.
GLUON_PRIORITY ?= 0

# Languages to include
GLUON_LANGS ?= en de
